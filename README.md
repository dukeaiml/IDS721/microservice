[![pipeline status](https://gitlab.com/dukeaiml/IDS721/zhankai_ye_mini_project_3/-/wikis/uploads/8b9be60c3dabf89331bc0c118cae1e44/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/zhankai_ye_individual_project_2/-/commits/main)
# Color Guessing Rust Microservice
In this project, I've enhanced the design and functionality of the "Guess the Color" game. In this game, the server randomly chooses a color from a predefined list, and the player tries to guess the color by clicking on one of several buttons displayed on a webpage. Each button corresponds to a different color option. Upon clicking a button, the user is directed to a new page that informs them if their guess was right or wrong.

## Goals
* Simple REST API/web service in Rust
* Dockerfile to containerize service
* CI/CD pipeline files

## Steps
### Step 1: Initialize Rust Project
* `cargo new <YOUR-PROJECT-NAME>`
* Edit the `Cargo.toml` to include Actix Web and other dependencies.
* Implement Your Web App: Replace the content of `src/main.rs`. 
* Test Your Application using `cargo run`.

### Step 2: Create a Dockerfile
* Create a Dockerfile in the root of your project. This file will contain instructions for building the Docker image of your Actix web app.

### Step 3: Build the Docker Image
* Build the Docker Image. Open a terminal in the directory containing your Dockerfile and run: `docker build -t <YOUR-IMAGE-NAME> .`.
* Run the Container. After the image has been built, you can run it as a container. The following command runs the container and maps port 8080 inside the container to port 8080 on your host machine, allowing you to access the web application via `http://localhost:8080`. `docker run -p 8080:8080 <YOUR-IMAGE-NAME>`.

### Step 4: CI/CD Pipeline
* Enable the CI/CD pipeline by creating the .gitlab-ci.yml file.

## Demo
![Video Demo](https://gitlab.com/dukeaiml/IDS721/microservice/-/wikis/uploads/f1cd2f7eb4512eb2ec167af2cc9e60e5/IND2.mov)


