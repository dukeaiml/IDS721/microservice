use actix_files as fs;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;
use serde::Deserialize;

#[derive(Deserialize)]
struct Guess {
    color: String,
}

async fn index() -> impl Responder {
    fs::NamedFile::open("index.html").unwrap()
}

async fn guess_color(query: web::Query<Guess>) -> impl Responder {
    let colors = ["red", "blue", "green", "yellow"];
    let mut rng = rand::thread_rng();
    let selected_color = colors[rng.gen_range(0..colors.len())];

    let response_message = if query.color == selected_color {
        "Congratulations! You guessed correctly.".to_owned()
    } else {
        format!("Sorry, the color was {}. Try again!", selected_color)
    };

    HttpResponse::Ok()
        .content_type("text/html")
        .body(format!(
            r#"<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Guess the Color</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap" rel="stylesheet">
    <style>
        body {{
            font-family: 'Montserrat', sans-serif;
            background: linear-gradient(135deg, #667eea 0%, #764ba2 100%);
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            color: #333;
        }}
        .container {{
            text-align: center;
            background: white;
            padding: 40px;
            border-radius: 10px;
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
            width: 90%;
            max-width: 400px;
        }}
        .message {{
            color: {color};
            font-size: 20px;
            font-weight: 600;
            margin: 0 0 20px 0;
        }}
        a {{
            display: inline-block;
            margin-top: 20px;
            padding: 12px 25px;
            background-color: {color};
            color: white;
            text-decoration: none;
            border-radius: 5px;
            font-weight: 600;
            transition: background-color 0.3s ease;
        }}
        a:hover {{
            background-color: {hover_color};
        }}
    </style>
</head>
<body>
    <div class="container">
        <p class="message">{message}</p>
        <a href="/">Try Again</a>
    </div>
</body>
</html>"#,
            message = response_message,
            color = if query.color == selected_color { "#4CAF50" } else { "#f44336" },
            hover_color = if query.color == selected_color { "#81C784" } else { "#e57373" }
        ))
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index))
            .route("/guess", web::get().to(guess_color))
            .service(fs::Files::new("/static", ".").show_files_listing())
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}





